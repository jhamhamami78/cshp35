FROM ubuntu:18.04

RUN apt update -qq && apt install -y python python2.7 python-pip wget curl libpng16-16 libbz2-1.0 zlib1g -qq&&python2 -m pip install --upgrade pip&&pip install pyyaml requests tqdm colorama six requests_toolbelt

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

CMD ["printenv"]
